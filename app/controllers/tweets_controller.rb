class TweetsController < ApplicationController
    def index
        @tweets = Tweet.all
    end
    def new
        @tweet = Tweet.new
    end
    def create
        message = params[:tweet][:message]
        tdate = params[:tweet][:tdate]
        @tweet = Tweet.new(message: message, tdate: tdate)
        if @tweet.save
            flash[:notice] = "ツイートしました！"
            redirect_to root_path
        else
            flash[:failed] = "ツイートに失敗しました。"
            render 'new'
        end
    end
    def show
        @tweet = Tweet.find(params[:id])
    end
    def destroy
        tweet = Tweet.find(params[:id])
        tweet.destroy
            flash[:destroy] = "ツイートを削除しました。"
        redirect_to root_path, flash: {destroy:  "ツイートを削除しました。"}

    end
    def edit
         @tweet = Tweet.find(params[:id])
    end
    def update
        @tweet = Tweet.find(params[:id])
        @tweet.update(message: params[:tweet][:message],tdate: params[:tweet][:tdate])
        if @tweet.save
            flash[:update] = "更新しました！"
            redirect_to root_path
        else
            flash[:fail] = "更新に失敗しました。"
            render 'edit'
        end
    end
end
    
